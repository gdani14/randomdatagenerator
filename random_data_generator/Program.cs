﻿// See https://aka.ms/new-console-template for more information
using System.Formats.Asn1;
using System.Globalization;
using System;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Collections;

internal class Program
{
    static Random rnd = new Random();
    static List<Country> countryCodes = new List<Country>();
    static List<string> firstNames = new List<string>();
    static List<string> lastNames = new List<string>();
    static List<OutputData> generatedData = new List<OutputData>();

    private static void Main(string[] args)
    {
        var configuration = new CsvConfiguration(CultureInfo.InvariantCulture)
        {
            HasHeaderRecord = false,
            Delimiter = ";",
        };
        using (var reader = new StreamReader("country_codes.csv"))
        using (var csv = new CsvReader(reader, configuration))
        {
            countryCodes = csv.GetRecords<Country>().ToList();
        }
        firstNames = readData("anyakonyvezheto_utonevek_2019_08.csv");
        lastNames = readData("top100csaladnev2022.csv");


        firstNames = firstNames.Select(f => f[0].ToString().ToUpper() + f.Substring(1).ToLower()).ToList();
        lastNames = lastNames.Select(f => f[0].ToString().ToUpper() + f.Substring(1).ToLower()).ToList();


        DateTime dateTime = DateTime.Now;
        for (int i = 0; i < 2000000; i++)
        {
            generatedData.Add(new OutputData() { Name = generateName(), PhoneNumber = generatePhoneNumber() });
        }
        Console.WriteLine(DateTime.Now - dateTime);




        using (var writer = new StreamWriter("generated_data.csv"))
        using (var csvWriter = new CsvWriter(writer, configuration))
        {
            csvWriter.WriteRecords(generatedData);
        }



    }

    static List<string> readData(string path)
    {
        return File.ReadAllLines(path).ToList();
    }

    static string generatePhoneNumber()
    {
        var ccc = countryCodes[rnd.Next(countryCodes.Count)].CallingCode;
        var phoneNumber = ccc + " " + rnd.Next(10) + rnd.Next(10) + rnd.Next(10) + rnd.Next(10) + rnd.Next(10) + rnd.Next(10) + rnd.Next(10);
        return phoneNumber;
    }

    static string generateName()
    {
        var fn = firstNames[rnd.Next(firstNames.Count)];
        var ln = lastNames[rnd.Next(lastNames.Count)];
        return (ln + " " + fn);
    }
}

public class Country
{
    [Index(0)]
    public string? Name { get; set; }
    [Index(1)]
    public string? Code { get; set; }
    [Index(2)]
    public string? CallingCode { get; set; }
}
public class OutputData
{
    public string Name { get; set; }
    public string PhoneNumber { get; set; }
}